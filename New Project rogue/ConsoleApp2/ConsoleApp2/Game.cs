﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RLNET;
using RogueSharp;
using ConsoleApp2.Core;
using ConsoleApp2.Systems;
using RogueSharp.Random;

namespace ConsoleApp2
{
    public class Game
    {
        private static readonly int _screenWidth = 100;
        private static readonly int _screenHeight = 70;
        private static RLRootConsole rlRootConsole;

        private static readonly int _mapWidth = 80;
        private static readonly int _mapHeight = 48;
        private static RLConsole rlMapConsole;

        private static readonly int _messageWidth = 80;
        private static readonly int _messageHeight = 11;
        private static RLConsole rlMessageConsole;

        private static readonly int _statWidth = 20;
        private static readonly int _statHeight = 70;
        private static RLConsole rlStatConsole;

        private static readonly int _inventoryWidth = 80;
        private static readonly int _inventoryHeight = 11;
        private static RLConsole rlInventoryConsole;

        public static Player Player { get; set; }
        public static DungeonMap DungeonMap { get; private set; }

        private static bool _renderRequired = true;
        public static CommandSystem CommandSystem { get; private set; }

        public static MessageLog MessageLog { get; private set; }

        public static IRandom Random { get; private set; }

        public static void Main()
        {
            int seed = (int)DateTime.UtcNow.Ticks;
            Random = new DotNetRandom(seed);

            string fontFileName = "terminal8x8.png";

            string consoleTitle = $"Tutorial - Seed {seed}";

            rlRootConsole = new RLRootConsole(fontFileName, _screenWidth, _screenHeight, 8, 8, 1f, consoleTitle);

            rlMapConsole = new RLConsole(_mapWidth, _mapHeight);
            rlMessageConsole = new RLConsole(_messageWidth, _messageHeight);
            rlStatConsole = new RLConsole(_statWidth, _statHeight);
            rlInventoryConsole = new RLConsole(_inventoryWidth, _inventoryHeight);

            //Player = new Player();
            CommandSystem = new CommandSystem();

            MapGenerator mapGenerator = new MapGenerator(_mapWidth, _mapHeight,20,13,7);
            DungeonMap = mapGenerator.CreateMap();

            MessageLog = new MessageLog();
            MessageLog.Add("The rogue dude arrives in level 1");
            MessageLog.Add($"Level created with seed '{seed}'");

            DungeonMap.UpdatePlayerFieldOfView();


            // Moved from console render
            /*
            rlMessageConsole.SetBackColor(0, 0, _messageWidth, _messageHeight, Swatch.DbDeepWater);
            rlMessageConsole.Print(1, 1, "Messages", RLColor.White);*/

            //rlStatConsole.SetBackColor(0, 0, _statWidth, _statHeight, Swatch.DbOldStone);
            //rlStatConsole.Print(1, 1, "Stats", RLColor.White);

            rlInventoryConsole.SetBackColor(0, 0, _inventoryWidth, _inventoryHeight, Swatch.DbWood);
            rlInventoryConsole.Print(1, 1, "Inventory", RLColor.White);

            //

            rlRootConsole.Update += OnRootConsoleUpdate;
            rlRootConsole.Update += OnRootConsoleRender;
            rlRootConsole.Run();

            


        }

        private static void OnRootConsoleRender(object sender, UpdateEventArgs e)
        {
            if (_renderRequired)
            {
                rlMapConsole.Clear();
                rlStatConsole.Clear();
                rlMessageConsole.Clear();
                DungeonMap.Draw(rlMapConsole,rlStatConsole);
                Player.Draw(rlMapConsole, DungeonMap);
                
                MessageLog.Draw(rlMessageConsole);
                //Player.DrawStats(rlMessageConsole);

                Player.DrawStats(rlStatConsole);
                /*
                rlMapConsole.SetBackColor(0, 0, _mapWidth, _mapHeight, Colors.FloorBackground);
                rlMapConsole.Print(1, 1, "Map", RLColor.White);
                */

                RLConsole.Blit(rlMapConsole, 0, 0, _mapWidth, _mapHeight, rlRootConsole, 0, _inventoryHeight);
                RLConsole.Blit(rlStatConsole, 0, 0, _statWidth, _statHeight, rlRootConsole, _mapWidth, 0);
                RLConsole.Blit(rlMessageConsole, 0, 0, _messageWidth, _messageHeight, rlRootConsole, 0, _screenHeight - _messageHeight);
                RLConsole.Blit(rlInventoryConsole, 0, 0, _inventoryWidth, _inventoryHeight, rlRootConsole, 0, 0);


                rlRootConsole.Draw();
                _renderRequired = false;
            }
        }

        private static void OnRootConsoleUpdate(object sender, UpdateEventArgs e)
        {            

            bool didPlayerAct = false;
            RLKeyPress keyPress = rlRootConsole.Keyboard.GetKeyPress();
            if (keyPress != null)
            {
                if (keyPress.Key == RLKey.Up)
                {
                    didPlayerAct = CommandSystem.MovePlayer(Direction.Up);
                }
                else if (keyPress.Key == RLKey.Down)
                {
                    didPlayerAct = CommandSystem.MovePlayer(Direction.Down);
                }
                else if (keyPress.Key == RLKey.Left)
                {
                    didPlayerAct = CommandSystem.MovePlayer(Direction.Left);
                }
                else if (keyPress.Key == RLKey.Right)
                {
                    didPlayerAct = CommandSystem.MovePlayer(Direction.Right);
                }
                else if (keyPress.Key == RLKey.Escape)
                {
                    rlRootConsole.Close();
                }
            }

            if (didPlayerAct)
            {
                _renderRequired = true;
            }
        }
    }
    
}
